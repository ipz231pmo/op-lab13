﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#define ARRAY_SIZE 200000
#define AVERAGE_COUNT 10
#define BIG_ARRAY_SIZE
#define SHOW_SORT_TIME
void printArray(int array[]);
double shellSort(int array[]) {
    clock_t start = clock();
    int step = ARRAY_SIZE / 2;
    while (step > 0){
        for (int i = 0; i < (ARRAY_SIZE - step); i++){
            int j = i;
            while (j >= 0 && array[j] > array[j + step])
            {
                int c = array[j];
                array[j] = array[j + step];
                array[j + step] = c;
                j--;
            }
        }
        step = step / 2;
    }
    return (double)(clock() - start) / CLOCKS_PER_SEC;
}
double insertionSort(int array[]) {
    clock_t start = clock();
    int c;
    for (int i = 1; i < ARRAY_SIZE; i++)
    {
        c = array[i];
        for (int j = i - 1; j >= 0 && array[j] > c; j--)
        {
            array[j + 1] = array[j];
            array[j] = c;
        }
    }
    return (double)(clock() - start) / CLOCKS_PER_SEC;
}
void heapify(int arr[], int n, int i) {
    int largest = i;
    int left = i * 2 + 1;
    int right = i * 2 + 2;
    if (left < n && arr[left] > arr[largest]) largest = left;
    if (right < n && arr[right] > arr[largest]) largest = right;
    if (largest != i) {
        int temp = arr[largest];
        arr[largest] = arr[i];
        arr[i] = temp;
        heapify(arr, n, largest);
    }
}
double heapSort(int array[]) {
    clock_t start = clock();
    for (int i = ARRAY_SIZE / 2 - 1; i >= 0; i--)
        heapify(array, ARRAY_SIZE, i);
    for (int i = ARRAY_SIZE - 1; i >= 0; i--) {
        int temp = array[i];
        array[i] = array[0];
        array[0] = temp;
        heapify(array, i, 0);
    }
    return (double)(clock() - start) / CLOCKS_PER_SEC;
}
double selectionSort(int array[]) {
    clock_t start = clock();
    for (int i = 0; i < ARRAY_SIZE - 1; i++) {
        int imin = i;
        for (int j = i + 1; j < ARRAY_SIZE; j++)
            if (array[imin] > array[j]) imin = j;
        array[i] = array[i] + array[imin];
        array[imin] = array[i] - array[imin];
        array[i] = array[i] - array[imin];
    }
    return (double)(clock() - start) / CLOCKS_PER_SEC;
}
double bubbleSort(int array[]) {
    clock_t start = clock();
    int flag = 0;
    do {
        flag = 0;        
        for (int j = 0; j < ARRAY_SIZE - 1; j++) {
            if (array[j] > array[j + 1]) {
                int temp = array[j];
                array[j] = array[j + 1];
                array[j + 1] = temp;
                flag = 1;
            }
        }
    } while (flag);
    return (double)(clock() - start) / CLOCKS_PER_SEC;
}
void printArray(int array[]) {
    printf("array = { ");
    for (int i = 0; i < ARRAY_SIZE; i++) {
        printf("%3d ", array[i]);
    }
    printf("}\n");
}
void randomizeArray(int array[]) {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        array[i] = rand() % 201 - 100;
    }
}
typedef double (*fp)(int*);
void sortAverage(fp functionSort, int array[], const char* sortName) {
    double average = 0;
    for (int i = 0; i < AVERAGE_COUNT; i++) {
        randomizeArray(array);
#ifndef BIG_ARRAY_SIZE
        printArray(array);
#endif // !BIG_ARRAY_SIZE
        double t = functionSort(array);
        average += t;
#ifndef BIG_ARRAY_SIZE
        printArray(array);
#endif // !BIG_ARRAY_SIZE
#ifndef SHOW_SORT_TIME
        char resStr[100];
        strcpy(resStr, "Sorting time using ");
        strcat(resStr, sortName);
        strcat(resStr, " is %f seconds\n");
        printf(resStr, t);
#endif // !SHOW_SORT_TIME
    }
    average /= AVERAGE_COUNT;
    char resStr[100];
    strcpy(resStr, "Average sorting time using ");
    strcat(resStr, sortName);
    strcat(resStr, " is %f\n\n");
    printf(resStr, average);
}

int main() {
    srand(time(0));
    int arr[ARRAY_SIZE] = { 0 };    
    sortAverage(heapSort, arr, "Heap Sort");
    sortAverage(shellSort, arr, "Shell Sort");
    sortAverage(selectionSort, arr, "Selection Sort");
    sortAverage(insertionSort, arr, "Insertion Sort");
    sortAverage(bubbleSort, arr, "Bubble Sort");
    return 0;
}